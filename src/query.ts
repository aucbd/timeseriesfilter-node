import {
    And,
    Block,
    BlockClose,
    BlockOpen,
    FilterItem,
    FiltersList,
    Greater,
    GreaterEqual,
    Is,
    ListAnd,
    ListOr,
    Logic,
    Lower,
    LowerEqual,
    Not,
    Or,
    RangeInc,
    RangeLInc,
    RangeRInc,
    RegNot,
    Wildcard,
} from "./types";
import {intersperse} from "./utils";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import {getMongoQuery} from "sql2mongo";

export interface IField {
    field: string,
    join: (string | number)[],
    zero: number | string,
    type: "str" | "num"
}

interface ILinearDictionary {
    [index: string]: string,

    And: string,
    Or: string,
    Is: string,
    Not: string,
    RegIs: string,
    RegNot: string,
    BlockOpen: string,
    BlockClose: string,
    ListAnd: string,
    ListOr: string,
    Wildcard: string,
    Greater: string,
    GreaterEqual: string,
    Lower: string,
    LowerEqual: string,
}

const dictionarySQL: ILinearDictionary = {
    And: "AND",
    Or: "OR",
    Is: "=",
    Not: "!=",
    RegIs: "LIKE",
    RegNot: "NOT LIKE",
    BlockOpen: "(",
    BlockClose: ")",
    ListAnd: "AND",
    ListOr: "OR",
    Wildcard: "%",
    Greater: ">",
    GreaterEqual: ">=",
    Lower: "<",
    LowerEqual: "<=",
}
const dictionarySQL2Mongo: ILinearDictionary = {
    ...dictionarySQL,
    Wildcard: "*"
}

export type DatabaseType = "sql" | "mongo"

function joinValue(dictionary: ILinearDictionary, field: IField, values: string[], forceZero: boolean = false): string {
    values = intersperse(values, field.join.map(String))

    if (field.type === "str") {
        return JSON.stringify(values
            .map((v) =>
                v.replace("*", forceZero ? field.zero.toString() : dictionary[Wildcard.name]))
            .join(""))
    } else if (field.type === "num") {
        return Number(values
            .map((v) => v.replace("*", field.zero.toString()))
            .join(""))
            .toString()
    }

    return ""
}

function queryLinearRange(dictionary: ILinearDictionary, fields: Map<string, IField>, filter: FilterItem): string {
    const field = fields.get(filter.field) as IField
    const values = filter.values
    let rangeA = filter.type.is(RangeInc) || filter.type.is(RangeLInc) ? new GreaterEqual() : new Greater()
    let rangeB = filter.type.is(RangeInc) || filter.type.is(RangeRInc) ? new LowerEqual() : new Lower()
    let logic = new And()

    rangeA = filter.equality.is(Not) || filter.equality.is(RegNot) ? rangeA.not() : rangeA
    rangeB = filter.equality.is(Not) || filter.equality.is(RegNot) ? rangeB.not() : rangeB
    logic = filter.equality.is(Not) || filter.equality.is(RegNot) ? logic.not() : logic

    if (values[0].length > 0 && values[1].length > 0) {
        return [
            field.field, dictionary[rangeA.name], joinValue(dictionary, field, values[0], true),
            dictionary[logic.name],
            field.field, dictionary[rangeB.name], joinValue(dictionary, field, values[1], true)
        ].join(" ")
    } else if (values[0].length > 0) {
        return [field.field, dictionary[rangeA.name], joinValue(dictionary, field, values[0], true)].join(" ")
    } else {
        return [field.field, dictionary[rangeB.name], joinValue(dictionary, field, values[1], true)].join(" ")
    }
}

function queryLinearList(dictionary: ILinearDictionary, fields: Map<string, IField>, filter: FilterItem): string {
    const field = fields.get(filter.field) as IField
    const values = filter
        .values
        .map((v) =>
            joinValue(dictionary, field, v, filter.equality.is(Is) || filter.equality.is(Not)))
        .map((v) => `${field.field} ${dictionary[filter.equality.name]} ${v}`)

    return intersperse(values, [dictionary[filter.type.name]]).join(" ")
}

function queryLinearFilter(dictionary: ILinearDictionary, fields: Map<string, IField>, filter: FilterItem): string {
    if (!fields.has(filter.field)) {
        throw Error(`Missing field ${filter.field}`)
    } else if (filter.type.is(ListAnd) || filter.type.is(ListOr)) {
        return queryLinearList(dictionary, fields, filter)
    } else {
        return queryLinearRange(dictionary, fields, filter)
    }
}

function queryLinearItem(dictionary: ILinearDictionary, fields: Map<string, IField>, item: Logic | Block | FilterItem): string {
    if (item.is(And) || item.is(Or) || item.is(BlockOpen) || item.is(BlockClose)) {
        return dictionary[item.name]
    } else if (item.is(FilterItem)) {
        return [
            dictionary[BlockOpen.name],
            queryLinearFilter(dictionary, fields, item as FilterItem),
            dictionary[BlockClose.name]
        ].join(" ")
    } else {
        throw new TypeError(`Unknown filter item ${item.name}`)
    }
}

export function query(databaseType: DatabaseType, fields: Map<string, IField>, filters: FiltersList): string {
    if (!filters.length) {
        return ""
    } else if (databaseType === "sql") {
        return filters
            .map((f) => queryLinearItem(dictionarySQL, fields, f))
            .join(" ")
    } else if (databaseType === "mongo") {
        return JSON.stringify((getMongoQuery as (a: string) => Record<string, unknown>)(
            filters
                .map((f) => queryLinearItem(dictionarySQL2Mongo, fields, f))
                .join(" ")))
    } else {
        throw new TypeError(`Unknown databaseType ${databaseType as string}`)
    }
}