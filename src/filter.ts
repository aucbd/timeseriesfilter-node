import {Language} from "./language";
import {IField, query, DatabaseType} from "./query";
import {FiltersList} from "./types";
import {parse} from "./parse";

export class Filter {
    constructor(language: Language) {
        this.language = language
        this.parsedFilters = []
    }

    public fromString(parseString: string): FiltersList {
        this.parsedFilters = parse(this.language, parseString)

        return this.parsedFilters
    }

    public toString(): string {
        return this.parsedFilters
            .map(String)
            .join(" ")
    }

    public toQuery(databaseType: DatabaseType, fields: Map<string, IField>): string {
        return query(databaseType, fields, this.parsedFilters)
    }

    public language: Language
    public parsedFilters: FiltersList
}