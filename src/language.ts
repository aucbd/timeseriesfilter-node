function regStr(r: RegExp) {
    return r.source
}

export interface IFilterField {
    key: string,
    base: RegExp,
    wildcards: RegExp,
    groups: RegExp,
    groupsWildcards: RegExp
}

export interface IFilterFieldExtended extends IFilterField {
    range: RegExp,
    list: RegExp
}

export interface IOperators {
    is: RegExp,
    not: RegExp,
    regIs: RegExp,
    regNot: RegExp,
    and: RegExp,
    or: RegExp,
    list: RegExp,
    rangeInc: RegExp,
    rangeLInc: RegExp,
    rangeRInc: RegExp,
    rangeNonInc: RegExp,
    blockOpen: RegExp,
    blockClose: RegExp,
}

interface IOperatorsTypes {
    equality: RegExp[],
    logic: RegExp[],
    range: RegExp[],
    list: RegExp[],
    block: RegExp[],
    split: RegExp[]
}

interface IPatterns {
    validCharacters: RegExp,
    invalidCharacters: RegExp,
    language: RegExp,
    splitRange: RegExp,
    splitList: RegExp,
    split: RegExp,
    filter: RegExp,
    fields: Map<string, IFilterFieldExtended>
}

export class Language {
    public fields: Map<string, IFilterField>
    public operators: IOperators
    public operatorsTypes: IOperatorsTypes
    public patterns: IPatterns

    constructor(fields: IFilterField[], operators: IOperators) {
        this.fields = new Map(fields.map((f) => [f.key, f]))
        this.operators = operators
        this.operatorsTypes = {
            equality: [operators.is, operators.not, operators.regIs, operators.regNot],
            logic: [operators.and, operators.or],
            range: [operators.rangeInc, operators.rangeRInc, operators.rangeLInc, operators.rangeNonInc],
            list: [operators.list],
            block: [operators.blockOpen, operators.blockClose],
            split: [operators.and, operators.or, operators.blockOpen, operators.blockClose]
        }
        this.patterns = {
            validCharacters: /./,
            invalidCharacters: /./,
            language: /./,
            splitRange: /./,
            splitList: /./,
            split: /./,
            filter: /./,
            fields: new Map<string, IFilterFieldExtended>(),
        }

        this.build()
    }

    public build(): void {
        const buildValidCharacters: string = [
            /A-Za-z0-9/,
            /\/:./,
            /\*/,
            ...this.operatorsTypes.equality,
            ...this.operatorsTypes.list,
            ...this.operatorsTypes.range,
            ...this.operatorsTypes.logic,
            ...this.operatorsTypes.block
        ].map(regStr).join("")

        const buildFields: IFilterFieldExtended[] = Array(...this.fields.values())
            .map((field) => ({
                    ...field,
                    range: RegExp(
                        "(" +
                        `(${field.base.source})(${this.operatorsTypes.range.map(regStr).join("|")})(${field.base.source})?` +
                        "|" +
                        `(${field.base.source})?(${this.operatorsTypes.range.map(regStr).join("|")})(${field.base.source})` +
                        ")"
                    ),
                    list: RegExp(`(${field.wildcards.source})(${this.operators.list.source}(${field.wildcards.source}))*`)
                } as IFilterFieldExtended)
            )

        const buildFieldsPatterns: string[] = buildFields
            .map((field) =>
                `${field.key}(${this.operatorsTypes.equality.map(regStr).join("|")})(${field.range.source}|${field.list.source})`)

        const buildLanguageFilters: RegExp = RegExp(`(${buildFieldsPatterns.join("|")})`)

        this.patterns.validCharacters = RegExp(`[${buildValidCharacters}]`, "i")
        this.patterns.invalidCharacters = RegExp(`[^${buildValidCharacters}]`, "i")
        this.patterns.language = RegExp(
            "^(" +
            `(${this.operators.blockOpen.source})*` +
            `${buildLanguageFilters.source}` +
            `(${this.operators.blockClose.source})*` +
            `(${this.operatorsTypes.logic.map(regStr).join("|")}|$)` +
            ")+$",
            "i"
        )
        this.patterns.splitRange = RegExp(`(${this.operatorsTypes.range.map(regStr).join("|")})`)
        this.patterns.splitList = RegExp(`(${this.operatorsTypes.list.map(regStr).join("|")})`)
        this.patterns.split = RegExp(`(${this.operatorsTypes.split.map(regStr).join("|")})`)
        this.patterns.filter = buildLanguageFilters
        this.patterns.fields = new Map(buildFields.map((f) => [f.key, f]))
    }
}
