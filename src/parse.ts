import {IFilterFieldExtended, Language} from "./language";
import {
    And,
    Block,
    BlockClose,
    BlockOpen,
    FilterItem,
    FiltersList,
    Is,
    ListAnd,
    ListOr,
    Logic,
    Not,
    Or,
    RangeInc,
    RangeLInc,
    RangeNonInc,
    RangeRInc,
    RegIs,
    RegNot,
} from "./types";
import {remove} from "./utils";

function parseValues(fieldPatterns: IFilterFieldExtended, valuesString: string): string[] {
    const valuesMatch = fieldPatterns.groupsWildcards.exec(valuesString)
    if (valuesMatch !== null) {
        return valuesMatch
            .slice(1)
            .map((v) => (v || "*").replace(/\*+/, "*"))
    } else {
        return []
    }
}

function parseFilter(language: Language, filterString: string): FilterItem | null {
    const filter: FilterItem = new FilterItem(
        filterString.charAt(0),
        new Is()
    )

    if (language.operators.is.test(filterString.charAt(1))) filter.equality = new Is()
    else if (language.operators.regIs.test(filterString.charAt(1))) filter.equality = new RegIs()
    else if (language.operators.not.test(filterString.charAt(1))) filter.equality = new Not()
    else if (language.operators.regNot.test(filterString.charAt(1))) filter.equality = new RegNot()

    const filterStringValues: string = filterString.slice(2)
    const fieldPatterns: IFilterFieldExtended | undefined = language.patterns.fields.get(filter.field)

    if (fieldPatterns === undefined) return null

    if (fieldPatterns.range.test(filterStringValues)) {
        const splitResult = filterStringValues.split(language.patterns.splitRange)
        const value1 = splitResult[0]
        const rangeType = splitResult[1]
        const value2 = splitResult[2]

        if (language.operators.rangeInc.test(rangeType)) filter.type = new RangeInc()
        else if (language.operators.rangeRInc.test(rangeType)) filter.type = new RangeRInc()
        else if (language.operators.rangeLInc.test(rangeType)) filter.type = new RangeLInc()
        else if (language.operators.rangeNonInc.test(rangeType)) filter.type = new RangeNonInc()

        filter.values = [parseValues(fieldPatterns, value1), parseValues(fieldPatterns, value2),]
    } else if (fieldPatterns.list.test(filterStringValues)) {
        const valuesString = filterStringValues.split(language.patterns.splitList)
        filter.type = filter.equality.is(Is) || filter.equality.is(RegIs) ? new ListOr() : new ListAnd()
        filter.values = remove(valuesString, 1).map((v) => parseValues(fieldPatterns, v))
    } else {
        return null
    }

    return filter
}

function parseItem(language: Language, item: string): Logic | Block | FilterItem | null {
    if (language.operators.and.test(item)) return new And()
    else if (language.operators.or.test(item)) return new Or()
    else if (language.operators.blockOpen.test(item)) return new BlockOpen()
    else if (language.operators.blockClose.test(item)) return new BlockClose()
    else if (language.patterns.filter.test(item)) return parseFilter(language, item)
    return null
}

export function parse(language: Language, parseString: string): FiltersList {
    parseString = parseString.replace(language.patterns.invalidCharacters, "")

    if (!language.patterns.language.test(parseString)) return []
    // eslint-disable-next-line @typescript-eslint/prefer-regexp-exec
    else if ((parseString.match(language.operators.blockOpen) || []).length !== (parseString.match(language.operators.blockClose) || []).length) {
        return []
    }

    return parseString
        .split(language.patterns.split)
        .filter((f) => f.length > 0)
        .map((f) => parseItem(language, f))
        .filter((f) => f !== null) as (Logic | Block | FilterItem)[]
}