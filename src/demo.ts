import * as tsfilter from "./index";
import * as readline from "readline";

const fieldTime: tsfilter.IFilterField = {
    key: "T",
    base: /\d{1,2}:\d{1,2}:\d{1,2}\.\d{1,3}|\d{1,2}:\d{1,2}:\d{1,2}|\d{1,2}:\d{1,2}|\d{1,2}/,
    wildcards: /[\d*]{1,2}:[\d*]{1,2}:[\d*]{1,2}\.[\d*]{1,3}|[\d*]{1,2}:[\d*]{1,2}:[\d*]{1,2}|[\d*]{1,2}:[\d*]{1,2}|[\d*]{1,2}/,
    groups: /(\d)?(\d):?(\d)?(\d)?:?(\d)?(\d)?\.?(\d)?(\d)?(\d)?/,
    groupsWildcards: /([\d*])?([\d*]):?([\d*])?([\d*])?:?([\d*])?([\d*])?\.?([\d*])?([\d*])?([\d*])?/
}

const fieldWeekday: tsfilter.IFilterField = {
    key: "W",
    base: /[1-7]/,
    wildcards: /[1-7]/,
    groups: /([1-7])/,
    groupsWildcards: /([1-7])/,
}

const operators: tsfilter.IOperators = {
    is: /=/,
    not: /!/,
    regIs: /~/,
    regNot: /\^/,
    and: /&/,
    or: /\|/,
    list: /,/,
    rangeInc: /-/,
    rangeLInc: />/,
    rangeRInc: /</,
    rangeNonInc: /_/,
    blockOpen: /\(/,
    blockClose: /\)/,
}

const fields: Map<string, tsfilter.IField> = new Map<string, tsfilter.IField>([
    ["T", {field: "TIME", join: ["", ":", "", ":", "", ".", ""], zero: "0", type: "str"} as tsfilter.IField],
    ["W", {field: "DAY", join: [""], zero: "0", type: "num"} as tsfilter.IField],
])

const language = new tsfilter.Language([fieldTime, fieldWeekday], operators)
const filter = new tsfilter.Filter(language)
const read = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});


function prompt() {
    read.question("Insert filter string: ", (answer) => {
        switch (answer.trim()) {
            case "":
                console.log("EXIT")
                read.close()
                return
            default:
                filter.fromString(answer.trim())
                console.log("ParsedFilters: ", filter.toString())
                console.log("SQL Query    : ", filter.toQuery("sql", fields))
                console.log("Mongo Query  : ", filter.toQuery("mongo", fields))
                console.log()
                prompt()
        }
    })
}

prompt()
