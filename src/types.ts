class BaseType {
    public toString = (): string => this.name
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    public is = (other: any): other is Type => this instanceof other

    public name: string = this.constructor.name
}

export class Is extends BaseType {
    public not = (): Not => new Not()
}

export class Not extends BaseType {
    public not = (): Is => new Is()
}

export class RegIs extends BaseType {
    public not = (): RegNot => new RegNot()
}

export class RegNot extends BaseType {
    public not = (): RegIs => new RegIs()
}

export class And extends BaseType {
    public not = (): Or => new Or()
}

export class Or extends BaseType {
    public not = (): And => new And()
}

export class Greater extends BaseType {
    public not = (): LowerEqual => new LowerEqual()
}

export class GreaterEqual extends BaseType {
    public not = (): Lower => new Lower()
}

export class Lower extends BaseType {
    public not = (): GreaterEqual => new GreaterEqual()
}

export class LowerEqual extends BaseType {
    public not = (): Greater => new Greater()
}

export class BlockOpen extends BaseType {
    public not = (): BlockClose => new BlockClose()
}

export class BlockClose extends BaseType {
    public not = (): BlockOpen => new BlockOpen()
}

export class ListAnd extends BaseType {
    public not = (): ListOr => new ListOr()
}

export class ListOr extends BaseType {
    public not = (): ListAnd => new ListAnd()
}

export class RangeInc extends BaseType {
}

export class RangeRInc extends BaseType {
}

export class RangeLInc extends BaseType {
}

export class RangeNonInc extends BaseType {
}

export class Wildcard extends BaseType {
}

export class FilterItem extends BaseType {
    public field: string
    public equality: Equality
    public type: Range | List
    public values: string[][]

    constructor(field: string, equality: Equality, type: Range | List = new ListOr(), values: string[][] = []) {
        super()
        this.field = field
        this.equality = equality
        this.type = type
        this.values = values
    }

    public toString = (): string => `{${this.field} ${this.equality.name} ${this.type.name} ${JSON.stringify(this.values)}}`
}

export type Logic = And | Or
export type Equality = Is | Not | RegIs | RegNot
export type Inequality = Greater | GreaterEqual | Lower | LowerEqual
export type Range = RangeInc | RangeRInc | RangeLInc | RangeNonInc
export type List = ListAnd | ListOr
export type Block = BlockOpen | BlockClose
export type Type = Equality | Inequality | Logic | Range | List | Block
export type FiltersList = (Logic | Block | FilterItem)[]
