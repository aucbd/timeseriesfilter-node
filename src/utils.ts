export function remove<A>(list: A[], index: 1 | 0): A[] {
    return list.filter((_, i) => i % 2 !== index)
}

export function intersperse<A>(list: A[], delim: A[]): A[] {
    if (!list.length) return []

    return list
        .map((x) => [x])
        .reduce((a, b, i) =>
            a.concat(delim.length > i - 1 ? delim[i - 1] : delim[delim.length - 1], b))
}
